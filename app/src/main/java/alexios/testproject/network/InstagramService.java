package alexios.testproject.network;

import alexios.testproject.model.instagram.MediaResponse;
import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

public interface InstagramService {

    static final String INSTA_TOKEN = "get_a_token_alex"; //needs to get a token

    @GET("/v1/users/self/media/recent?access_token=" + INSTA_TOKEN)
    Observable<MediaResponse> getPersonalPhotos();

    @GET("/v1/users/{userId}/media/recent/")
    Observable<MediaResponse> getUserPhotos(@Path("userId") String user);
}
