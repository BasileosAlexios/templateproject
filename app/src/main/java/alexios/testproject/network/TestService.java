package alexios.testproject.network;

import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Observable;

public interface TestService {

    @GET("data/{id}/")
    Observable<Object> getData(@Path("id") String token);

    @FormUrlEncoded
    @POST("data/{id}/")
    Observable<Object> postData(@Path("id") String token, @Body Object bodyOfData);
}


