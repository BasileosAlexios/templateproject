package alexios.testproject.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import javax.inject.Inject;

import alexios.testproject.CustomApplication;
import alexios.testproject.R;
import alexios.testproject.presenter.MainPresenter;
import alexios.testproject.views.MainActivityViewInterface;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends FragmentActivity implements MainActivityViewInterface {

    @BindView(R.id.fab)
    FloatingActionButton fab;

    @Inject
    MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        ((CustomApplication) getApplication()).getActivityComponent().inject(this);
        presenter.bindView(this);
    }

    @OnClick(R.id.fab)
    public void onTap() {
        presenter.doAction();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void doViewAction() {
        Toast.makeText(this, "Booh! Action performed!", Toast.LENGTH_SHORT);
    }

    @Override
    public Context getContext() {
        return this.getContext();
    }
}
