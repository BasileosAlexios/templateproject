package alexios.testproject.injection;

import javax.inject.Singleton;

import alexios.testproject.activities.MainActivity;
import dagger.Component;

@Singleton
@Component(modules = {NetworkModule.class, ApplicationModule.class})
public interface ActivityComponent {

    void inject(MainActivity activity);
}

