package alexios.testproject.injection;

import android.app.Activity;
import android.support.annotation.Nullable;

import javax.inject.Singleton;

import alexios.testproject.CustomApplication;
import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    private final CustomApplication application;

    private Activity currentActivity;

    public ApplicationModule(CustomApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    CustomApplication provideApplication() {
        return this.application;
    }

    @Provides
    @Nullable
    Activity provideCurrentActivity() {
        return currentActivity;
    }

    public void setCurrentActivity(Activity a) {
        currentActivity = a;
    }

}
