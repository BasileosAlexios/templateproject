package alexios.testproject.injection;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;

import com.squareup.sqlbrite.SqlBrite;

import org.chalup.microorm.MicroOrm;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DatabaseModule {

    private final SQLiteOpenHelper sqlOpenHelper;
    private SqlBrite sqlBrite;
    private MicroOrm uOrm;

    public DatabaseModule(SQLiteOpenHelper sqLiteOpenHelper) {
        this.sqlOpenHelper = sqLiteOpenHelper;
    }

    @Provides
    @Singleton
    SqlBrite providesSqlBrite() {
        return getSqlBrite();
    }

    private SqlBrite getSqlBrite() {
        if (sqlBrite == null) {
            sqlBrite = SqlBrite.create(sqlOpenHelper);
            sqlBrite.setLoggingEnabled(true);
        }
        return sqlBrite;
    }


    @Provides
    @Singleton
    MicroOrm providesMicroOrm() {
        return getMicroOrm();
    }

    private MicroOrm getMicroOrm() {
        if (uOrm == null) {
            uOrm = new MicroOrm();
        }
        return uOrm;
    }


}
