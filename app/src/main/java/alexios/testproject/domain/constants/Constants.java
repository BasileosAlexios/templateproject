package alexios.testproject.domain.constants;

import java.util.concurrent.TimeUnit;

public class Constants {

	public static final String BASE_URL		= "http://exemplo.com";

	public static final String BASE_URL_WITH_SLASH = BASE_URL + "/";

	public static final String SERVICE_ENDPOINT_URL_V1 = BASE_URL_WITH_SLASH + "api/v1/";

	public static final String INSTAGRAM_SERVICE_ENDPOINT = "https://api.instagram.com/v1/";

}
