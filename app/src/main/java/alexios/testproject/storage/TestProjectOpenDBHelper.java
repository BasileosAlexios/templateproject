package alexios.testproject.storage;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

public class TestProjectOpenDBHelper extends SQLiteOpenHelper {

    private static final int VERSION = 1;

    private static final String CREATE_TABLE = "";
//            + "CREATE TABLE IF NOT EXISTS " + Model.TABLE + "("
//            + Model.ID + " INTEGER NOT NULL PRIMARY KEY ON CONFLICT REPLACE, "
//            + Model.NAME + " TEXT NOT NULL DEFAULT '' "
//            + ")";

    private static SQLiteDatabase _db;

    public TestProjectOpenDBHelper(Context context) {
        super(context, "testapp.db", null, VERSION);
        _db = getWritableDatabase();
    }

    public boolean hasTableExist(SQLiteDatabase db, String tableName) {
        Cursor cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" + tableName + "'", null);

        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.close();
                return true;
            }
            cursor.close();
        }
        return false;
    }

    @Override
    public void onCreate(@NonNull SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(@NonNull SQLiteDatabase db, int oldVersion, int newVersion) {
       // if (!BuildConfig.DEBUG) {
            //db.execSQL("DROP TABLE IF EXISTS " + Model.TABLE);
            onCreate(db);
       // }
    }

    public static void deleteAllDatabases(boolean reCreate) {
       // _db.execSQL("DROP TABLE IF EXISTS " + Model.TABLE);


        if (reCreate) {
            _db.execSQL(CREATE_TABLE);
        }
    }
}