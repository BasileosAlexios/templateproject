package alexios.testproject.helpers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

import static java.text.DateFormat.SHORT;
import static java.text.DateFormat.getDateInstance;
import static java.text.DateFormat.getTimeInstance;

public class DateFormatHelper {

    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final String DATE_AND_TIME_FORMAT = "yyyy-MM-dd hh:mm";
    public static final String TIME_FORMAT = "hhmm";
    private static final Locale locale = new Locale(System.getProperty("user.language"));

    public static String getTodaysDateAsString(){
        SimpleDateFormat format = new SimpleDateFormat(DateFormatHelper.DATE_FORMAT);
        return format.format(new Date());
    }

    public static Date convertTimeZone(long timestamp, TimeZone fromTimeZone, TimeZone toTimeZone) {
        Date date = new Date(timestamp);
        long fromTimeZoneOffset = getTimeZoneUTCAndDSTOffset(date, fromTimeZone);
        long toTimeZoneOffset = getTimeZoneUTCAndDSTOffset(date, toTimeZone);
        return new Date(timestamp + (toTimeZoneOffset - fromTimeZoneOffset));
    }

    private static long getTimeZoneUTCAndDSTOffset(Date date, TimeZone timeZone) {
        long timeZoneDSTOffset = 0;
        if (timeZone.inDaylightTime(date)) {
            timeZoneDSTOffset = timeZone.getDSTSavings();
        }

        return timeZone.getRawOffset() + timeZoneDSTOffset;
    }

    public static String getDateAsString(Date date) {
        String formattedDate = null;
        java.text.DateFormat dateFormat;
        Calendar adCalendar = Calendar.getInstance();
        Calendar nowCalendar = Calendar.getInstance();
        adCalendar.setTime(date);
        nowCalendar.setTime(new Date());
        if (isSameDay(adCalendar, nowCalendar)) {
            dateFormat = getTimeInstance(SHORT, locale);
            formattedDate = "Today, " + dateFormat.format(date);
        } else {
            dateFormat = getDateInstance(SHORT, locale);
            formattedDate = dateFormat.format(date);
            dateFormat = getTimeInstance(SHORT, locale);
            formattedDate = formattedDate + ", " + dateFormat.format(date);
        }
        return formattedDate;
    }

    private static boolean isSameDay(Calendar cal1, Calendar cal2) {
        return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
    }
}
