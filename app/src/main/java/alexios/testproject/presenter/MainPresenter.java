package alexios.testproject.presenter;

import javax.inject.Inject;

import alexios.testproject.CustomApplication;
import alexios.testproject.model.instagram.MediaResponse;
import alexios.testproject.network.InstagramService;
import alexios.testproject.views.MainActivityViewInterface;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Alexios on 1/30/2017.
 */
public class MainPresenter extends BasePresenter<MainActivityViewInterface> {


    @Inject
    InstagramService instagramService;

    @Inject
    public MainPresenter() {
        super();

    }


    @Override
    public void bindView(MainActivityViewInterface view) {
        super.bindView(view);
    }


    public void doAction(){

        testFunction("192.167.237.2").subscribe(new Subscriber<MediaResponse>() {
            @Override
            public void onError(Throwable e) {

                e.printStackTrace();

            }

            @Override
            public void onNext(MediaResponse object) {
                view.doViewAction();
            }

            @Override
            public void onCompleted() {

            }
        });;


    }


    private Observable<MediaResponse> testFunction(String s) {
        return instagramService.getPersonalPhotos()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }


}
