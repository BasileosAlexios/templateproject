package alexios.testproject.presenter;

import android.os.SystemClock;
import android.support.annotation.NonNull;

import alexios.testproject.views.BaseViewInterface;


public abstract class BasePresenter<V extends BaseViewInterface> {

    protected V view;


    public BasePresenter() {

    }

    public void bindView(@NonNull V view) {
        this.view = view;
    }

    public void unbindView() {
        this.view = null;
    }


}
