package alexios.testproject.views;

import android.content.Context;

public interface BaseViewInterface {

    Context getContext();

}
