package alexios.testproject.views;

/**
 * Created by Alexios on 1/30/2017.
 */
public interface MainActivityViewInterface extends BaseViewInterface {

    void doViewAction();
}
