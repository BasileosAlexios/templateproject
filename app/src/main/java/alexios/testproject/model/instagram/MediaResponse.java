package alexios.testproject.model.instagram;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MediaResponse {

    public static final String DATA = "data";

    @SerializedName(DATA)
    private List<Media> mMedia;

    public List<Media> getMedia() {
        return mMedia;
    }
}
