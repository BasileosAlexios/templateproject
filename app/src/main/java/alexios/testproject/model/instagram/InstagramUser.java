package alexios.testproject.model.instagram;

import com.google.gson.annotations.SerializedName;

import org.chalup.microorm.annotations.Column;

import java.io.Serializable;

public class InstagramUser implements Serializable {

    public static final String ID = "id";
    public static final String USERNAME = "username";
    public static final String FULLNAME = "full_name";
    public static final String PROFILE_PICTURE = "profile_picture";


    @SerializedName(USERNAME)
    @Column(USERNAME)
    private String mUsername;

    @SerializedName(ID)
    @Column(ID)
    private int mUserId;

    @SerializedName(FULLNAME)
    @Column(FULLNAME)
    private String mFullName;

    @SerializedName(PROFILE_PICTURE)
    @Column(PROFILE_PICTURE)
    private String mProfilePicture;

    public InstagramUser(int userId, String username, String fullName, String profilePicture) {
        mUserId = userId;
        mUsername = username;
        mFullName = fullName;
        mProfilePicture = profilePicture;
    }

    public String getUsername() {
        return mUsername;
    }

    public int getUserId() {
        return mUserId;
    }

    public String getFullName() {
        return mFullName;
    }

    public String getProfilePicture() {
        return mProfilePicture;
    }

    @Override
    public String toString() {
        return mUsername;
    }
}