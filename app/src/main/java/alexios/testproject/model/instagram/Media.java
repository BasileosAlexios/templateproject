package alexios.testproject.model.instagram;

import com.google.gson.annotations.SerializedName;

import org.chalup.microorm.annotations.Column;

public class Media {

    public static final String ID = "id";
    public static final String TYPE = "type";
    public static final String LINK = "link";
    public static final String LIKES = "likes";
    public static final String IMAGES = "images";
    public static final String CAPTION = "caption";
    public static final String USER = "user";


    @SerializedName(ID)
    @Column(ID)
    private String mId;

    @SerializedName(TYPE)
    @Column(TYPE)
    private String mMediaType;

    @SerializedName(LINK)
    @Column(LINK)
    private String mLink;

    @SerializedName(LIKES)
    @Column(LIKES)
    private Likes mLikes;

    @SerializedName(IMAGES)
    @Column(IMAGES)
    private ImageContainer mImageContainer;

    @SerializedName(CAPTION)
    @Column(CAPTION)
    private Caption mCaption;

    @SerializedName(USER)
    @Column(USER)
    private InstagramUser mUser;

    public String getId() {
        return mId;
    }

    public String getLink() {
        return mLink;
    }

    public String getUrl() {
        if (mImageContainer == null) {
            return null;
        }

        Image image = mImageContainer.getImage();
        if (image == null) {
            return null;
        }

        return image.getUrl();
    }

    public String getCaption() {
        if (mCaption == null) {
            return "";
        }

        return mCaption.getText();
    }

    public int getNumberOfLikes() {
        return mLikes.getMLikeCount();
    }

    public String getUsername() {
        if (mUser == null) {
            return "";
        }

        return mUser.getUsername();
    }

    public Image getThumbnail(){
        return mImageContainer.getmThumbnailImage();
    }

    public boolean isImageType() {
        return "image".equals(mMediaType);
    }

    public InstagramUser getUser() {
        return mUser;
    }

    public static class ImageContainer {

        public static final String STANDARD_RESOLUTION = "standard_resolution";
        public static final String THUMBNAIL = "thumbnail";

        @SerializedName(STANDARD_RESOLUTION)
        @Column(STANDARD_RESOLUTION)
        private Image mImage;

        @SerializedName(THUMBNAIL)
        @Column(THUMBNAIL)
        private Image mThumbnailImage;

        public Image getImage() {
            return mImage;
        }

        public Image getmThumbnailImage() {
            return mThumbnailImage;
        }
    }

    public static class Image {

        public static final String URL = "url";

        @SerializedName(URL)
        @Column(URL)
        private String mUrl;

        public String getUrl() {
            return mUrl;
        }
    }

    public class Caption {

        public static final String TEXT = "text";

        @SerializedName(TEXT)
        @Column(TEXT)
        private String mText;

        public String getText() {
            return mText;
        }
    }

    public class Likes {

        public static final String COUNT = "count";

        @SerializedName(COUNT)
        @Column(COUNT)
        private int mLikeCount;

        public int getMLikeCount() {
            return mLikeCount;
        }
    }
}
