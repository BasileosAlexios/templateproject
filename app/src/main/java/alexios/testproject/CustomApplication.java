package alexios.testproject;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import alexios.testproject.injection.ActivityComponent;
import alexios.testproject.injection.ApplicationModule;
import alexios.testproject.injection.DaggerActivityComponent;
import alexios.testproject.injection.NetworkModule;

public class CustomApplication extends Application {

    private static ActivityComponent activityComponent;
    private ApplicationModule applicationModule;

    public static CustomApplication get(Context context) {
        return (CustomApplication) context.getApplicationContext();
    }


    @Override
    public void onCreate() {
        super.onCreate();


        applicationModule = new ApplicationModule(this);
        // Instancia SQLite
//        TestProjectOpenDBHelper helper = new TestProjectOpenDBHelper(this);
////
        activityComponent = DaggerActivityComponent.builder()
                .applicationModule(applicationModule)
                .networkModule(new NetworkModule())
                //   .databaseModule(new DatabaseModule(helper))
                .build();


    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Log.i("Application", "Application.onLowMemory()");
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
    }

    public ActivityComponent getActivityComponent() {
        return activityComponent;
    }


}
